
.PHONY: default
default: build



.PHONY: all
all: clone build

.PHONY: build
build:
	./build.sh

.PHONY: clone
clone:
	./clone.sh
