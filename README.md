# Newsletter Webseite

Dieses Repo beinhaltet Metadaten, Templates und Skripte zum Bauen der AStA Newsletter Webseite + Trigger zum automatischen Bauen der AStA Newsletter Webseite.

Die gehostete Webseite findest du unter [newsletter.asta.uni-goettingen.de](https://newsletter.asta.uni-goettingen.de).

Die eigentlichen Newsletter findest du im [newsletter Repo](https://gitlab.gwdg.de/asta/dnd-referat/newsletter). Dort fügst du auch neue Newsletter hinzu.

## Mögliche Fehler
### Pipeline läuft erfolgreich, aber die Website bleibt unverändert
Die Website wird über Caprover deployed (hier: https://captain.service.asta.uni-goettingen.de/).
Dort kann nach einiger Zeit der Speicher auf dem Server voll sein. Wenn das passiert (zuletzt im Oktober 2024), wird im Caprover in den Build Logs nach apt-get update ein GPG-Error geworfen.
Lösung: Auf dem captain server den Befehl "sudo docker system prune -a" ausführen. 
Das löscht alle ungenutzten Docker Images. Der Disk-Cleanup im Caprover Dashboard löscht hingegen nur dangling images. Danach kann man eine neue Pipeline starten und es sollte alles wieder funktionieren.

### Pipeline scheitert wegen zu großen Artefakten
Früher wurden immer _alle_ Newsletter als Artefakt übergeben. Das hat 2024 zu einem Fehler geführt, der gefixt sein _sollte_, indem immer nur noch der neuste Ordner mit Newslettern (d.h. die aktuelle Legislatur) übergeben wird.

## Abhängigkeiten

- GNU Coreutils
- `make`
- `bash`
- `git`
- `python3`
- `python3-pip`
- `jq`
- `jinja2-cli` (über pip3: `pip3 install jinja2-cli`)
- `docker` (falls man den Dockercontainer selber bauen will)

## Lokal Hosten

0. (Überprüfen, dass man alle Abhängigkeiten hat.)
1. Zunächst muss man das Repo Klonen: `git clone git@gitlab.gwdg.de:asta/dnd-referat/newsletter-webseite.git && cd newsletter-webseite`
2. `make clone` ausführen.
3. `make build` ausführen.
4. Den `public/`-Ordner im Browser öffnen.

Ab jetzt kannst du Änderungen machen.
Sobald du `make` ausführst wird der Inhalt im `public/`-Ordner aktuallisiert.

