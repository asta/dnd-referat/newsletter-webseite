#!/bin/bash

# Exit on error
set -e

# Change working dir to dir of current script
pushd "$(dirname "$0")" >/dev/null

# Load config
source ./config


function main() {
	print_info "Starting build.sh. ($(date))"

	validate_all_newsletter_paths

	prepare_deploy_dir

	pushd "${DEPLOY_DIR}" >/dev/null
	
	build_legislatures_json | jq '.' > "${LEGISLATURES_JSON_FILE}"

	build_data_json | jq '.' > "${DATA_JSON_FILE}"

	copy_newsletter_html_files_to_index_html
	
	copy_newsletter_dirs_to_yyyy_mm_dd_version
	
	popd >/dev/null

	render_site_in_all_languages

	copy_default_language_to_index_html
	
	print_info "Finished build.sh. ($(date))"
}


function print_error_and_exit() {
	echo -e "\n\e[35m=> \e[31m[ERROR] ${1}\e[0m\n" >&2
	exit 1
}

function print_task() {
	echo -e "\e[35m=> \e[33m[TASK] \e[0m${1}\e[0m" >&2
}

function print_info() {
	echo -e "\e[35m=> \e[36m[INFO] \e[0m${1}\e[0m" >&2
}

function print_debug() {
	if [ "${ENABLE_DEBUG}" = "true" ]; then
		echo -e "\e[35m=> \e[34m[DEBUG] \e[0m${1}\e[0m" >&2
	fi
}

function validate_all_newsletter_paths() {
	print_task "Validating that all newsletter.html files have a parsable path"
	pushd "${NEWSLETTER_DIR}" >/dev/null

	local invalid_nlfiles_regex='^\.\/legislature-20[0-9][0-9]_[0-9][0-9]\/20[0-9][0-9]-[0-2][0-9]-[0-3][0-9]\/newsletter\.html$'
	local nlfiles="$(find . -type f | grep -v '\/\.git' | grep '\/newsletter\.html$')"
	local invalid_nlfiles="$(echo "${nlfiles}" | grep -v "${invalid_nlfiles_regex}")"
	if [ "${invalid_nlfiles}" != "" ]; then
		print_error_and_exit "Found files with an invalid path (not matching '${invalid_nlfiles_regex}'):\n${invalid_nlfiles}"
	fi
	popd >/dev/null
}

# Prepare deploy dir
function prepare_deploy_dir() {
	print_task "Preparing deploy dir"
	[ -d "${DEPLOY_DIR}" ] && print_task "Cleaning deploy dir" && rm -rf "${DEPLOY_DIR}"
	mkdir -p "${DEPLOY_DIR}"
	print_task "Copy newsletter files to deploy dir"
	find "${NEWSLETTER_DIR}" -mindepth 1 -maxdepth 1  -type d | grep -v '\/\.git' | sed "s,^,cp -rvf \",;s,$,\" \"${DEPLOY_DIR}/\"," | bash
	print_task "Copy static files to deploy dir"
	cp -rv "${STATIC_DIR}" "${DEPLOY_DIR}/"
	print_task "Copy ${LANG_JSON_FILE} to deploy dir"
	cp -v "${LANG_JSON_FILE}" "${DEPLOY_DIR}/"
}

function build_newsletter_json() {
	local legislature_dir="${1}"
	local legislature="${2}"
	local year="${3}"
	local month="${4}"
	local day="${5}"
	local newsletter_dir="${legislature_dir}/${year}-${month}-${day}"
	local base_url="${year}/${month}/${day}"
	
	print_info "Found Newsletter: ${newsletter_dir}"

	printf '{'
	printf '"%s": %s,' "year" "${year}"
	printf '"%s": %s,' "month" "${month}"
	printf '"%s": "%s",' "monthstr" "${month}"
	printf '"%s": %s,' "day" "${day}"
	printf '"%s": "%s",' "daystr" "${day}"
	printf '"%s": "%s",' "directory" "${newsletter_dir}"
	printf '"%s": "%s",' "file_name" "newsletter.html"
	if [ -f "${newsletter_dir}/${FILES_JSON_FILE}" ]; then
		printf '"%s":%s,' "has_extra_files" "true"
		printf '"%s":' "extra_files"
		cat "${newsletter_dir}/${FILES_JSON_FILE}"
		printf ',\n'
	else
		printf '"%s":%s,' "has_extra_files" "false"
	fi
	if [ -f "${newsletter_dir}/${NOTE_JSON_FILE}" ]; then
		printf '"%s":%s,' "has_note" "true"
		printf '"%s":' "note"
		cat "${newsletter_dir}/${NOTE_JSON_FILE}"
		printf ',\n'
	else
		printf '"%s":%s,' "has_note" "false"
	fi
	printf '"%s": "%s"' "base_url" "${base_url}"
	printf '}'
}

function build_month_json() {
	local legislature_dir="${1}"
	local legislature="${2}"
	local year="${3}"
	local month="${4}"
	local firstday="true"
	printf '{'
	printf '"%s": %s,' "month" "${month}"
	printf '"%s": "%s",' "monthstr" "${month}"
	printf '"%s": [' "newsletters"
	find "${legislature_dir}" -type f | grep -v '\/\.git' | grep '\/newsletter\.html$' | cut -d '/' -f 3 | grep "^${year}-${month}-" | cut -d '-' -f 3 | sort -u | while read day ; do
		if [ "${firstday}" = "true" ]; then
			firstday="false"
		else
			printf ','
		fi
		build_newsletter_json "${legislature_dir}" "${legislature}" "${year}" "${month}" "${day}"
	done
	printf ']'
	printf '}'
}

function build_year_json() {
	local legislature_dir="${1}"
	local legislature="${2}"
	local year="${3}"
	local firstmonth="true"
	printf '{'
	printf '"%s": %s,' "year" "${year}"
	printf '"%s": [' "months"
	find "${legislature_dir}" -type f | grep -v '\/\.git' | grep '\/newsletter\.html$' | cut -d '/' -f 3 | grep "^${year}-" | cut -d '-' -f 2 | sort -u | while read month ; do
		print_debug "Found month: ${month}"
		if [ "${firstmonth}" = "true" ]; then
			firstmonth="false"
		else
			printf ','
		fi
		build_month_json "${legislature_dir}" "${legislature}" "${year}" "${month}"
	done
	printf ']'
	printf '}'
}

function build_legislature_json() {
	local legislature_dir="${1}"
	local legislature="$(echo "${legislature_dir}" | cut -d '-' -f 2 | tr -d '/')"
	local firstyear="true"
	printf '{'
	printf '"%s": "%s",' "legislature_underscore" "${legislature}"
	printf '"%s": "%s",' "legislature_slash" "$(echo "${legislature}" | tr '_' '/')"
	printf '"%s": [' "years"
	find "${legislature_dir}" -type f | grep -v '\/\.git' | grep '\/newsletter\.html$' | cut -d '/' -f 3 | cut -d '-' -f 1 | sort -u | while read year ; do
		print_debug "Found year: ${year}"
		if [ "${firstyear}" = "true" ]; then
			firstyear="false"
		else
			printf ','
		fi
		build_year_json "${legislature_dir}" "${legislature}" "${year}"
	done
	printf ']'
	printf '}'

}

function build_legislatures_json() {
	print_task "Building legislatures json"
	local firstleg="true"
	local legislature_dir=""
	printf '['
	find "." -type f | grep -v '\/\.git' | grep '^\.\/legislature-' | grep '\/newsletter\.html$' | cut -d '/' -f 2 | sort -u | while read legislature_dir ; do
		print_debug "Found legislature directory: ./${legislature_dir}"
		if [ "${firstleg}" = "true" ]; then
			firstleg="false"
		else
			printf ','
		fi
		build_legislature_json "./${legislature_dir}"
	done
	printf ']'
}

function build_data_json() {
	print_task "Building data json"
	printf '{'

	printf '"%s": ' "lang"
	cat "${LANG_JSON_FILE}"

	printf ','

	printf '"%s": ' "legislatures"
	cat "${LEGISLATURES_JSON_FILE}"

	printf '}'
}

function render_site() {
	local sitelang="$1"
	print_task "Rendering site (language: ${sitelang})"
	jinja2 --format=json -D l="${sitelang}" --strict "${TEMPLATES_DIR}/site.html.j2" "${DEPLOY_DIR}/${DATA_JSON_FILE}" -o "${DEPLOY_DIR}/${sitelang}.html"
	print_task "Rendering atom feed (language: ${sitelang})"
	jinja2 --format=json -D l="${sitelang}" --strict "${TEMPLATES_DIR}/atom.xml.j2" "${DEPLOY_DIR}/${DATA_JSON_FILE}" -o "${DEPLOY_DIR}/atom-${sitelang}.xml"
}

function render_site_in_all_languages() {
	# Render site in different languages
	NUMSUPPORTEDLANGS="$(cat "${LANG_JSON_FILE}" | jq -j '.supported | length')"
	print_info "Currently ${NUMSUPPORTEDLANGS} supported languages found."
	for ((i = 0; i < ${NUMSUPPORTEDLANGS}  ; i++)); do
		CURLANG="$(cat "${LANG_JSON_FILE}" | jq -j ".supported[${i}]")"
		render_site "${CURLANG}"
	done
}

function copy_default_language_to_index_html() {
	# Copy site with default language to index.html
	DEFAULTLANG="$(cat "${LANG_JSON_FILE}" | jq -j ".default")"
	print_task "Copy site with default language (${DEFAULTLANG}) to index.html"
	cp -v "${DEPLOY_DIR}/${DEFAULTLANG}.html" "${DEPLOY_DIR}/index.html"
}

function copy_newsletter_html_files_to_index_html() {
	print_task "Copy newsletter html files to their corresponding index.html"
	local nlfile=""
	find "." -type f | grep -v '\/\.git' | grep '^\.\/legislature-' | grep '\/newsletter\.html$' | while read nlfile ; do
		cp -v "${nlfile}" "$(dirname "${nlfile}")/index.html"
	done
}
	
function copy_newsletter_dirs_to_yyyy_mm_dd_version() {
	local nlfile=""
	local nldir=""
	local nlyyyymmdd=""
	local year=""
	local month=""
	local day=""
	find "." -type f | grep -v '\/\.git' | grep '^\.\/legislature-' | grep '\/newsletter\.html$' | sort | while read nlfile ; do
		nldir="$(dirname "${nlfile}")"
		nlyyyymmdd="$(echo "${nldir}" | cut -d '/' -f 3)"
		year="$(echo "${nlyyyymmdd}" | cut -d '-' -f 1)"
		month="$(echo "${nlyyyymmdd}" | cut -d '-' -f 2)"
		day="$(echo "${nlyyyymmdd}" | cut -d '-' -f 3)"
		mkdir -p "${year}/${month}"
		cp -rv "${nldir}" "${year}/${month}/${day}"
		find "${nldir}" -mindepth 1 -maxdepth 1 | sed "s,^,cp -rv \",;s,\$,\" \"${year}/${month}/\"," | bash
	done
}

# Run main
main $@

# Undo "Change working dir to dir of current script"
popd >/dev/null
