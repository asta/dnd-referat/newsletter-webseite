FROM ubuntu:latest

COPY . /app
WORKDIR /app

RUN apt-get update && \
    DEBIAN_FRONTEND=noninteractive apt-get install -y nginx jq bash coreutils git python3 python3-pip make python3-venv

# Erstelle ein virtuelles Umfeld und installiere jinja2-cli
RUN python3 -m venv /opt/venv && \
    /opt/venv/bin/pip install jinja2-cli

# Setze das PATH für das virtuelle Umfeld
ENV PATH="/opt/venv/bin:$PATH"

RUN make all

RUN rm -vf /etc/nginx/sites-enabled/*
RUN ln -s /app/nginx.conf /etc/nginx/sites-enabled/newsletter.conf

# Überprüfe die Nginx-Konfiguration
RUN /usr/sbin/nginx -t

EXPOSE 80

ENTRYPOINT ["/usr/sbin/nginx", "-g", "daemon off; master_process on;"]
