#!/bin/bash

# Exit on error
set -e

# Load config
source ./config

if [ -d "${NEWSLETTER_DIR}" ]; then
	pushd "${NEWSLETTER_DIR}" >/dev/null

	# Pull Newsletter Repo
	git pull --rebase=true

	popd >/dev/null
else
	# Clone Newsletter Repo
	git clone "${NEWSLETTER_REPO_URL}" "${NEWSLETTER_DIR}"
fi



